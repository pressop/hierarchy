Hierarchy
=============

Object hierarchy logic (parent - children).

## Install

```
composer require pressop/hierarchy
```

## License

This bundle is under the MIT license. See the complete license:

    LICENSE
