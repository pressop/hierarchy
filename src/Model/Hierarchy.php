<?php

/*
 * This file is part of the pressop/hierarchy package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pressop\Component\Hierarchy\Model;

/**
 * Class Hierarchy
 *
 * @author Benjamin Georgeault
 */
class Hierarchy implements EditableHierarchyInterface
{
    use EditableHierarchyTrait;
}
