<?php

/*
 * This file is part of the pressop/hierarchy package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pressop\Component\Hierarchy\Model;

/**
 * Trait HierarchyTrait
 *
 * @author Benjamin Georgeault
 */
trait HierarchyTrait // implements HierarchyInterface
{
    /**
     * @var HierarchyInterface|null
     */
    protected $parent;

    /**
     * @var HierarchyInterface[]
     */
    protected $children = [];

    /**
     * @inheritdoc
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->children);
    }

    /**
     * @inheritdoc
     */
    public function count()
    {
        return count($this->children);
    }

    /**
     * @inheritdoc
     */
    public function getParent(): ?HierarchyInterface
    {
        return $this->parent;
    }

    /**
     * @inheritdoc
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @inheritdoc
     */
    public function hasChild(HierarchyInterface $child): bool
    {
        return in_array($child, $this->children);
    }
}
