<?php

/*
 * This file is part of the pressop/hierarchy package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pressop\Component\Hierarchy\Model;

/**
 * Interface HierarchyInterface
 *
 * @author Benjamin Georgeault
 */
interface HierarchyInterface extends \IteratorAggregate, \Countable
{
    /**
     * @return null|HierarchyInterface
     */
    public function getParent(): ?HierarchyInterface;

    /**
     * @return HierarchyInterface[]
     */
    public function getChildren();

    /**
     * @param HierarchyInterface $child
     * @return bool
     */
    public function hasChild(HierarchyInterface $child): bool;
}
