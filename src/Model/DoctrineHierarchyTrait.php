<?php

/*
 * This file is part of the pressop/hierarchy package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pressop\Component\Hierarchy\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * Trait DoctrineHierarchyTrait
 *
 * @author Benjamin Georgeault
 * @see DbHierarchyInterface
 */
trait DoctrineHierarchyTrait // implements DbHierarchyInterface
{
    /**
     * @var HierarchyInterface|null
     */
    protected $parent;

    /**
     * @var HierarchyInterface[]|Collection
     */
    protected $children;

    /**
     * @inheritdoc
     */
    public function getIterator()
    {
        return $this->getChildren()->getIterator();
    }

    /**
     * @inheritdoc
     */
    public function count()
    {
        return $this->getChildren()->count();
    }

    /**
     * @inheritdoc
     */
    public function getParent(): ?HierarchyInterface
    {
        return $this->parent;
    }

    /**
     * @return HierarchyInterface[]|Collection
     */
    public function getChildren()
    {
        return $this->children ? : $this->children = new ArrayCollection();
    }

    /**
     * @inheritdoc
     */
    public function hasChild(HierarchyInterface $child): bool
    {
        return $this->getChildren()->containsKey($child);
    }
}
