<?php

/*
 * This file is part of the pressop/hierarchy package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pressop\Component\Hierarchy\Model;

/**
 * Trait DoctrineEditableHierarchyTrait
 *
 * @author Benjamin Georgeault
 */
trait DoctrineEditableHierarchyTrait // implements DbHierarchyInterface, EditableHierarchyInterface
{
    use DoctrineHierarchyTrait;

    /**
     * @inheritdoc
     */
    public function setParent(HierarchyInterface $parent): HierarchyInterface
    {
        $this->parent = $parent;

        if (!$parent->hasChild($this) && $parent instanceof EditableHierarchyInterface) {
            $parent->addChild($this);
        }

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function addChild(HierarchyInterface $child): HierarchyInterface
    {
        if ($this->hasChild($child)) {
            throw new \InvalidArgumentException(sprintf('Child already in the collection.'));
        }

        $this->getChildren()->add($child);

        if ($this !== $child->getParent() && $child instanceof EditableHierarchyInterface) {
            $child->setParent($this);
        }

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function remove(HierarchyInterface $child): HierarchyInterface
    {
        if (!$this->hasChild($child)) {
            throw new \OutOfBoundsException(sprintf('Child not found in the collection.'));
        }

        $this->getChildren()->removeElement($child);

        return $this;
    }
}
