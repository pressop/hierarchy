<?php

/*
 * This file is part of the pressop/hierarchy package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pressop\Component\Hierarchy\Doctrine\ORM\Subscriber;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LoadClassMetadataEventArgs;
use Doctrine\ORM\Events;
use Doctrine\ORM\Mapping\ClassMetadata;
use Pressop\Component\Hierarchy\Model\DbHierarchyInterface;

/**
 * Class MappingSubscriber
 *
 * @author Benjamin Georgeault
 */
class MappingSubscriber implements EventSubscriber
{
    /**
     * @inheritDoc
     */
    public function getSubscribedEvents()
    {
        return [
            Events::loadClassMetadata,
        ];
    }

    /**
     * @param LoadClassMetadataEventArgs $eventArgs
     * @throws \Doctrine\ORM\Mapping\MappingException
     */
    public function loadClassMetadata(LoadClassMetadataEventArgs $eventArgs)
    {
        $metadata = $eventArgs->getClassMetadata();

        if (null === $metadata->reflClass) {
            return;
        }

        $reflectionClass = $metadata->getReflectionClass();
        if ($reflectionClass->implementsInterface(DbHierarchyInterface::class)) {
            $this->mappingHierarchy($metadata);
        }
    }

    /**
     * @param ClassMetadata $metadata
     * @throws \Doctrine\ORM\Mapping\MappingException
     */
    private function mappingHierarchy(ClassMetadata $metadata)
    {
        if (!$metadata->hasAssociation('parent')) {
            $class = $metadata->getReflectionClass()->getName();

            $metadata->mapManyToOne([
                'fieldName' => 'parent',
                'targetEntity' => $class,
                'inversedBy' => 'children',
            ]);

            $metadata->mapOneToMany([
                'fieldName' => 'children',
                'targetEntity' => $class,
                'mappedBy' => 'parent',
            ]);
        }
    }
}
